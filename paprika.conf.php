<?php
// The folder where your photo are uploaded
define('IMAGES_DIR', 'photos');

// The folder where the cached files (thumbnails and exif indexes) will be stored.
define('CACHE_DIR', 'cache');

// The size of the square thumbnails. Any size changes will requires you to edit the style.css file.
define('THUMB_HEIGHT', '150');

// Directive for the Cache-Control header (max-age is in seconds).
define('THUMB_CACHE_CONTROL', 'max-age=3600');

// If true, paprika will deal with the Last-Modified and If-Modified-Since headers. If false, no caching headers will be managed.
define('MANAGE_THUMB_LAST_MODIFIED', true); 
?>
