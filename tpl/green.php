<?php
/** Dark Template for Paprika **/

require_once 'paprika.inc.php';
require_once 'paprika.conf.php';

function displayCategories() {
  $categories = listCategories(IMAGES_DIR);
  
  foreach ($categories as &$category) {
    echo '<div class="item">';
    echo '<a href="' . $category['uri'] . '"><h2>' . $category['name'] . '</h2>';
    echo '<img class="b-lazy" src="spinner.gif" data-src="' . $category['cover-uri'] . '" alt="' . $category['name'] . '" width="' . THUMB_HEIGHT . '" height="' . THUMB_HEIGHT . '" />';
    echo '</a></div>';
  }
}

function displayPhotos($category) {
  $photos = listPhotos(IMAGES_DIR, $category);
  
  echo '<div id="breadcrumbs"><a href=".">Albums</a> <span id="selected-category">&gt; ' . $category . '</span></div>';
  foreach ($photos as $id => &$photo) {
  	echo '<div class="item">';
    echo '<a href="' . $photo['uri'] . '" class="photo">';
    echo '<img class="b-lazy" src="spinner.gif" data-href="' . $photo['uri'] . '" data-src="' . $photo['thumb-uri'] . '" alt="' . $photo['uri'] . '" width="' . THUMB_HEIGHT . '" height="' . THUMB_HEIGHT . '" />';
    echo '</a></div>';
  }
}

function displayGallery() {
  if (isset($_GET['cat']) && empty($_GET['cat']) === false) {
    displayPhotos($_GET['cat']);
  } else {
    displayCategories();
  }
}
?>


<!DOCTYPE html>
<html>
<!-- Hey ! Stop looking at my code, pervert ! -->
<head>
  <meta charset="utf-8" />
  <title>Paprika Gallery</title>
  <meta name="robots" content="all" />
  <meta name="author" content="Benjamin Billet" />
  <meta name="viewport" content="width=device-width, user-scalable=yes, initial-scale=1.0" />
  <meta name="description" content="Photographies" />
  <meta name="keywords" content="photographie, photo, art, image" />
  <link rel="stylesheet" href="tpl/green-style.css" type="text/css" media="screen" />
</head>

<body>
<div id="page">
<h1><a href=".">&#x1f336;Paprika</a></h1>

<p class="credits">
This is <strong>Paprika</strong>, the minimalistic photo gallery.
</p>

<div id="container"><?php displayGallery(); ?></div>
<div id="slideshow">
	<img src="spinner.gif" id="slideshow-image" alt="Loading..." />
	<div id="slideshow-previous">&lt;</div><div id="slideshow-next">&gt;</div>
	<div id="slideshow-caption"></div>
</div>
<div id="bottom">~ Powered by <a href="https://bitbucket.org/bbillet/paprika">Paprika</a>, the &micro;-weight gallery engine ~</div>
</div>

<script src="slideshow.js"></script>
<script src="blazy.js"></script>
<script type="text/javascript">
window.addEventListener('load', function(){
	var bLazy = new Blazy();
}, false);
</script>
</body>
</html>
