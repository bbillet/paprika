var items = [];
var current = 0;

window.addEventListener('load', function() {
  items = document.getElementsByClassName('photo');
  window.addEventListener('keydown', onKeyDown);
  document.getElementById('slideshow').addEventListener('click', hideSlideshow);
  document.getElementById('slideshow-image').addEventListener('click', showFullImage);
  document.getElementById('slideshow-image').addEventListener('load', onImageLoad);
  document.getElementById('slideshow-previous').addEventListener('click', previousImage);
  document.getElementById('slideshow-next').addEventListener('click', nextImage);
  
  var len = items.length;
  if (items != null && len > 0) {
    for (let i = 0; i < len; i++) {
      items[i].addEventListener('click', function(e) {
        blockEvent(e); 
        showSlideshow(i); 
      });
    }
  }
}, false);



function onKeyDown(e) {
  if (document.getElementById('slideshow').style.display != 'block') {
    return true;
  }
  
  if (e.keyCode == '37') {
    previousImage(e);
  } else if (e.keyCode == '39') {
    nextImage(e);
  }
}

function showSlideshow(start) {
  current = start;
  updateImage();
  
  var slideshow = document.getElementById('slideshow');
  slideshow.style.display = 'block';
}

function hideSlideshow(start) {  
  var slideshow = document.getElementById('slideshow');
  slideshow.style.display = 'none';
  
  var image = document.getElementById('slideshow-image');
  image.src = 'spinner.gif';
}

function nextImage(e) {
  blockEvent(e);
  
  current = (current + 1) % items.length;
  updateImage();
}

function previousImage(e) {
  blockEvent(e);

  current--;
  if (current < 0) {
    current = items.length - 1;
  }
  updateImage();
}

function updateImage() {
  var image = document.getElementById('slideshow-image');
  var caption = document.getElementById('slideshow-caption');
  
  caption.innerHTML = '';
  image.src = 'spinner.gif';
  
  image.src = items[current].childNodes[0].dataset.href;
}

function onImageLoad(e) {
  var image = document.getElementById('slideshow-image');
  if (image.src.endsWith('spinner.gif')) {
    return;
  }
  
  image.exifdata = null;
  
  EXIF.getData(image, function() {
    var exif = EXIF.getAllTags(image, this);
    var caption = document.getElementById('slideshow-caption');
    caption.innerHTML = buildCaption(exif);
  });
}

function showFullImage(e) {
  blockEvent(e);

  var imageUrl = items[current].childNodes[0].dataset.href;
  window.location = imageUrl;
}

function blockEvent(e) {
  e.stopPropagation();
  e.preventDefault();
}

function buildCaption(exif) {
  if (exif == null || Object.keys(exif).length === 0) {
    return '';
  }
  
  var author = exif.Artist || exif.Copyright;
  var width = exif.PixelXDimension;
  var height = exif.PixelYDimension;
  var maker = exif.Make;
  var model = exif.Model;
  if (maker && model) {
  	if (model.toLowerCase().startsWith(maker.toLowerCase())) {
      maker = null;
    } else if (maker.toLowerCase().startsWith('nikon corporation') && model.toLowerCase().startsWith('nikon')) {
      maker = null;
    }
  }
  
  var lensMaker = exif.LensMake;
  var lensModel = exif.LensModel;
  if (lensMaker && lensModel) {
  	if (lensModel.toLowerCase().startsWith(lensMaker.toLowerCase())) {
      lensMaker = null;
    }
  }
  
  
  var f = exif.FNumber;
  if (f == null && exif.ApertureValue) {
    // ApertureValue (Av) = 2 log2 (f), f = 2^(Av / 2)
    f = Number(Math.pow(2, exif.ApertureValue / 2).toFixed(1));
  }
  if (f === 0) {
    f = null;
  }
  
  var speed = exif.ExposureTime;
  if (speed == null && exif.ShutterSpeedValue) {
    // ShutterSpeedValue (Tv) = -log2 (exposure time), exposure time = 2^(-Tv)
    speed = Number(Math.pow(2, -1 * exif.ShutterSpeedValue).toFixed(1));
  }
  if (speed === 0) {
    speed = null;
  }
  
  var focal = parseInt(exif.FocalLength);
  if (focal == 0 || focal == NaN) {
    focal = null;
  }
  
  var iso = parseInt(exif.ISOSpeedRatings);
  if (iso == 0 || iso == NaN) {
    iso = null;
  }
  
  var items = [];
  if (width && height) {
    items.push(width + 'x' + height);
  }
  if (f) {
    items.push('f/' + f);
  }
  if (speed) {
    if (speed < 1) {
      speed = speed.numerator + '/' + speed.denominator;
    }
    items.push(speed + 's');
  }
  if (focal) {
    items.push(focal + 'mm');
  }
  if (iso) {
    items.push('ISO' + iso);
  }
  
  if (maker && model) {
    items.push(maker + ' ' + model);
  } else if (maker) {
    items.push(maker);
  } else if (model) {
    items.push(model);
  }
  if (lensMaker && lensModel) {
    items.push(lensMaker + ' ' + lensModel);
  } else if (lensMaker) {
    items.push('Lens: ' + lensMaker);
  } else if (lensModel) {
    items.push('Lens: ' + lensModel);
  }
  
  if (author != null && items.length > 0) {
    return author + ' | ' + items.join(', ');
  } else if (author) {
    return author;
  } else if (items.length > 0) {
    return items.join(', ');
  }
  return '';
}

