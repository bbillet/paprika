<?php
function startsWith($haystack, $needle, $case = true) {
  if ($case) {
    return (strcmp(substr($haystack, 0, strlen($needle)), $needle) === 0);
  }
  return (strcasecmp(substr($haystack, 0, strlen($needle)), $needle) === 0);
}

function endsWith($haystack, $needle, $case = true) {
  if ($case) {
    return (strcmp(substr($haystack, strlen($haystack) - strlen($needle)), $needle) === 0);
  }
  return (strcasecmp(substr($haystack, strlen($haystack) - strlen($needle)), $needle) === 0);
}

// serialize anything into a file, using the serialize() php function
function serializeToFile($object, $file, $chmod = 0705, $b64 = false, $deflate = false) {  
  $data = serialize($object);
  if($deflate) {
    $data = gzdeflate($data);
  }
  
  if($b64) {
    $data = base64_encode($data);
  }
  
  file_put_contents($file, '<?php /* ' . $data . ' */ ?>');  
  @chmod($file, $chmod);
}

// serialize anything into a file, using the serialize() php function
function unserializeFromFile($file, $b64 = false, $deflate = false) {
  if (is_file($file) == false) {
    return null;
  }

  $data = file_get_contents($file);
  $data = substr($data, strlen('<?php /* '), -strlen(' */ ?>'));
  if($b64) {
    $data = base64_decode($data);
  }
    
  if($deflate) {
    $data = gzinflate($data);
  }
  
  return unserialize($data);
}

// retrieve the first image of a directory
function getCover($dir) {
  if (is_dir($dir) === false) {
    return array();
  }
  $dh = opendir($dir);
  if ($dh === false) {
    return array();
  }
  
  while (($entry = readdir($dh)) !== false) {
    $filePath = $dir . '/' . $entry;
    if (is_file($filePath)) {
      return $filePath;
    }
  }
  closedir($dh);
}

// list the category folders contained in a directory and returns an array of structures:
// array('name' => <name of the category>, 'cover-uri' => <uri of the category cover>, 'uri' => <uri for opening the category>)
function listCategories($dir) {
  if (is_dir($dir) === false) {
    return array();
  }
  $dh = opendir($dir);
  if ($dh === false) {
    return array();
  }
  
  $categories = array();
  while (($entry = readdir($dh)) !== false) {
    $categoryDir = $dir . '/' . $entry;
    if ($entry !== '.' && $entry !== '..' && is_dir($categoryDir)) {    
      array_push($categories, array(
        'name' => $entry,
        'cover-uri' => 'paprika-thumb.php?' . http_build_query(array('file' => getCover($categoryDir))),
        'uri' => '?' . http_build_query(array('cat' => $entry)),
      ));
    }
  }
  
  closedir($dh);
  
  return $categories;
}

// list the photos contained in a folder and returns an array of structures:
// array('uri' => <uri of the photo file>, 'thumb-uri' => <uri of the thumbnail file>, 'caption' => <image caption, composed of exif data>)
function listPhotos($dir, $category) {
  $categoryDir = $dir . '/' . $category;
  if (is_dir($categoryDir) === false) {
    return null;
  }
  $dh = opendir($categoryDir);
  if ($dh === false) {
    return null;
  }
  
  $photos = array();
  while (($entry = readdir($dh)) !== false) {
    $photoPath = $categoryDir . '/' . $entry;
    if (is_file($photoPath)) {
      array_push($photos, array(
        'uri' => $photoPath,
        'thumb-uri' => 'paprika-thumb.php?' . http_build_query(array('file' => $photoPath))
      ));
    }
  }
  
  closedir($dh);
  
  return $photos;
}

// create a thumbnail from an image
function createThumbnail($image, $outputDir, $thumbSize = 150) {
  $imageSize = getimagesize($image);
  $width = $imageSize[0];
  $height = $imageSize[1];

  $isJpg = endsWith($image, '.jpg', false) || endsWith($image, '.jpeg', false);
  $isPng = endsWith($image, '.png', false);
  $isGif = endsWith($image, '.gif', false);
  
  $source = null;
  if ($isJpg) {
    $source = imagecreatefromjpeg($image);
  } else if ($isPng) {
    $source = imagecreatefrompng($image);
  } else if ($isGif) {
    $source = imagecreatefromgif($image);
  } else {
    return false;
  }

  if ($isJpg) {
    $exif = @exif_read_data($image);
    $rotation = 0;
    $flip = false;

    if ($exif != null) {
      $orientation = null;
      if (isset($exif['IFD0']) && isset($exif['Orientation'])) {
        $orientation = $exif['IFD0']['Orientation'];
      } else if (isset($exif['Orientation'])) {
        $orientation = $exif['Orientation'];
      }
      
      if ($orientation == 6 || $orientation == 5) {
        $rotation = 270;
      } else if ($orientation == 3 || $orientation == 4) {
        $rotation = 180;
      } else if ($orientation == 8 || $orientation == 7) {
        $rotation = 90;
      }

      if ($orientation == 5 || $orientation == 4 || $orientation == 7) {
        $flip = true;
      }

      if ($rotation !== 0) {
        $source2 = imagerotate($source, $degrees, 0);
        imagedestroy($source);
        $source = $source2;
      }
      if ($flip) {
        $source2 = imageflip($source, IMG_FLIP_HORIZONTAL);
        imagedestroy($source);
        $source = $source2;
      }
    }
  }
  
  $x = 0;
  $y = 0;

  if ($width > $height) { // horizontal picture
    $x = ceil(($width - $height) / 2);
    $width = $height;
  } else {
    $y = ceil(($height - $width) / 2);
    $height = $width;
  }
  
  $result = imagecreatetruecolor($thumbSize, $thumbSize);
  imagecopyresampled($result, $source, 0, 0, $x, $y, $thumbSize, $thumbSize, $width, $height);
  
  if (is_dir($outputDir) === false) {
    mkdir($outputDir, 0777, true);
  }
  
  $thumbFile = $outputDir . '/thumbs_' . basename($image);
  if ($isJpg) {
    imagejpeg($result, $thumbFile, 90);
    imagejpeg($result, null, 90);
  } else if ($isPng) {
    imagepng($result, $thumbFile, 90);
    imagepng($result, null, 90);
  } else if ($isGif) {
    imagegif($result, $thumbFile, 90);
    imagegif($result, null, 90);
  }
  
  imagedestroy($source);
  imagedestroy($result);
  
  return is_file($thumbFile);
}
?>
