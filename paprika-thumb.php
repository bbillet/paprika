<?php
require_once 'paprika.inc.php';
require_once 'paprika.conf.php';

if (isset($_GET['file']) === false) {
  http_response_code(400);
  die('You must provide a file parameter');
}

$file = $_GET['file'];

if (startsWith($file, IMAGES_DIR) === false || is_file($file) === false) {
  http_response_code(404);
  die('The requested image does not exist in the image store');
}

$isJpg = endsWith($file, '.jpg', false) || endsWith($file, '.jpeg', false);
$isPng = endsWith($file, '.png', false);
$isGif = endsWith($file, '.gif', false);

if (empty(THUMB_CACHE_CONTROL) == false) {
  header('Cache-Control: ' . THUMB_CACHE_CONTROL, true);
}

if (MANAGE_THUMB_LAST_MODIFIED) {
  $updateTime = filemtime($file);
  if ($updateTime !== false) {
    $headers = apache_request_headers();
    if (isset($headers['If-Modified-Since']) && (strtotime($headers['If-Modified-Since']) == $updateTime)) {
      header('Last-Modified: ' . gmdate('D, d M Y H:i:s', $updateTime) . ' GMT', true, 304);
      exit();
    } else {
      header('Last-Modified: ' . gmdate('D, d M Y H:i:s', $updateTime) . ' GMT', true);
    }
  }
}

if ($isJpg) {
  header('Content-type: image/jpeg');
} else if ($isPng) {
  header('Content-type: image/png');
} else if ($isGif) {
  header('Content-type: image/gif');
} else {
  http_response_code(400);
  die('Only jpeg, png and gif files are supported');
}

if (is_dir(CACHE_DIR) == false) {
  mkdir(CACHE_DIR);
}

$cachedFile = CACHE_DIR . '/thumb_' . basename($file);
if(is_file($cachedFile)) {
  if ($isJpg) {
    $cachedData = imagecreatefromjpeg($cachedFile);
    ImageJPEG($cachedData, null, 90);
  } else if ($isPng) {
    $cachedData = imagecreatefrompng($cachedFile);
    ImagePNG($cachedData, null, 90);
  } else if ($isGif) {
    $cachedData = imagecreatefromgif($cachedFile);
    ImageGIF($cachedData, null, 90);
  }
  exit;
}

$status = createThumbnail($file, CACHE_DIR, THUMB_HEIGHT);
if ($status === false) {
  http_response_code(500);
  die('Unable to generate the thumbnail image');
}
?>
